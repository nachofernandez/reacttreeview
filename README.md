# Tree View 

Base project created by using [Create React App](https://github.com/facebookincubator/create-react-app)

## Tech stack

- [React & React DOM](https://facebook.github.io/react/)
- [Bootstrap](https://http://getbootstrap.com/)
- [Jest (Testing Framework) ](https://facebook.github.io/jest)


## Available Scripts

In the project directory, you can run:

### `npm install`

Install development dependencies

### `npm start`

Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

### `npm test`

Launches the test runner in the interactive watch mode.  
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
