import React, { Component } from 'react';
import TreeView from './TreeView';
import './App.css';

const items =  {
    label: 'Node Label',
    items: [
        {
            label: 'Child Node Label',
            items: [
                {
                    label: 'Child Node Label',
                    items: [
                        {
                            label: 'Child Node Label',
                            items: [

                            ]
                        },
                        {
                            label: 'Child Node Label',
                        },
                        {
                            label: 'Leaf Node Label'
                        }
                    ]
                }
            ]
        },
        {
            label: 'Leaf Node Label'
        }
    ]
};

class App extends Component {
  render() {
    return (
      <div className="container">
          <div className="row">
              <div className="col-md-offset-4 col-md-4">
                <TreeView items={items} expand={true}/>
              </div>
          </div>
      </div>
    );
  }
}

export default App;
