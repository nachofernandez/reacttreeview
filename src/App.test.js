import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import App from './App';
import TreeView from './TreeView';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

it('Render Single Node Tree', () => {
  const items =  {
    label: 'Node Label',
    items: []
  };

  const treeView = TestUtils.renderIntoDocument(
      <TreeView items={items} expand={true}/>
  );

  var nodes = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'node');
  expect(nodes.length).toEqual(0);

  var leafs = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'leaf');
  expect(leafs.length).toEqual(1);
});


it('Render Node with Child expanded', () => {
  const items =  {
    label: 'Node Label',
    items: [
      {
        label: 'Child Node Label',
        items: []
      },
      {
        label: 'Leaf Node Label'
      }
    ]
  };

  const treeView = TestUtils.renderIntoDocument(
      <TreeView items={items} expand={true}/>
  );

  var nodes = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'node');
  expect(nodes.length).toEqual(1);

  var leafs = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'leaf');
  expect(leafs.length).toEqual(2);
});

it('Render Node with Child Collapsed', () => {
  const items =  {
    label: 'Node Label',
    items: [
      {
        label: 'Child Node Label',
        items: []
      },
      {
        label: 'Leaf Node Label'
      }
    ]
  };

  const treeView = TestUtils.renderIntoDocument(
      <TreeView items={items} expand={false}/>
  );

  var nodes = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'node');
  expect(nodes.length).toEqual(1);

  var leafs = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'leaf');
  expect(leafs.length).toEqual(0);
});

it('Expand/Collapse Node', () => {
  const items =  {
    label: 'Node Label',
    items: [
      {
        label: 'Child Node Label',
        items: []
      },
      {
        label: 'Leaf Node Label'
      }
    ]
  };

  const treeView = TestUtils.renderIntoDocument(
      <TreeView items={items} expand={false}/>
  );

  var headerLink = TestUtils.scryRenderedDOMComponentsWithTag(treeView, 'a');
  var leafs = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'leaf');
  expect(leafs.length).toEqual(0);

  TestUtils.Simulate.click(headerLink[0]);

  leafs = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'leaf');
  expect(leafs.length).toEqual(2);

  TestUtils.Simulate.click(headerLink[0]);

  leafs = TestUtils.scryRenderedDOMComponentsWithClass(treeView, 'leaf');
  expect(leafs.length).toEqual(0);
});