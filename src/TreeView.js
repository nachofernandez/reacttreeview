import React, { Component } from 'react';

class TreeView extends Component {
    constructor(props) {
        super(props);
        this.state = {expanded: this.props.expand, childExpanded: this.props.expand};
    }

    _renderChilds() {
        return this.props.items.items.map((item, index) => {
            let uniqueId = index + new Date().getUTCMilliseconds();
            return <TreeView key={uniqueId} items={item} expand={this.state.childExpanded}/>;
        });
    }

    _toogleExpanded(e) {
        e.preventDefault();
        this.setState({expanded: !this.state.expanded, childExpanded: false});
    };

    _renderIcon() {
        return this.state.expanded ? <span className="glyphicon glyphicon-minus" /> : <span className="glyphicon glyphicon-plus"/>;
    }

    render() {
        let hasChilds = this.props.items.items && this.props.items.items.length;
        return (hasChilds ?
            <div className="node">
                <a href="#" onClick={this._toogleExpanded.bind(this)}>{this._renderIcon()}</a><strong>{this.props.items.label}</strong>
                {this.state.expanded ? this._renderChilds() : null}
            </div> :
            <div className="leaf"><strong>{this.props.items.label}</strong></div>);
    }
}

TreeView.propTypes = {
    items: React.PropTypes.object,
    expand: React.PropTypes.bool
};

export default TreeView;